#!/bin/bash

if [ -z "$1" ]
then
    echo "MountPoint in ini"
    echo "----------------"
    MOUNTPOINT=`cat mtv.ini | grep MountPoint | sed 's/MountPoint = //'`
    echo $MOUNTPOINT
    echo ""
    
    TARGET="localhost"
    
    echo "ifconfig"
    echo "----------------"
    ifconfig
    echo ""
else
 if [ -z "$2" ]
    then
        echo "Missing MOUNTPOINT"
        exit 1
    else
        echo "Target Host"
        echo "----------------"
        TARGET=$1
        echo $TARGET
        echo ""
    
        echo "MountPoint"
        echo "----------------"
        MOUNTPOINT=$2
        echo $MOUNTPOINT
        echo ""
        
        echo "ifconfig"
        echo "----------------"
        echo "SKIP"
        echo ""

    fi
fi

echo "rpcinfo -p $TARGET"
echo "----------------"
rpcinfo -p $TARGET
echo ""

echo "rpcinfo -t $TARGET nfs"
echo "----------------"
rpcinfo -t $TARGET nfs
NFS_OK="$?"
echo ""

if [[ "$NFS_OK" == 0 ]]
then

    echo "showmount -e $TARGET"
    echo "----------------"
    showmount -e $TARGET
    echo ""
    
    echo "mkdir NFSTEST"
    echo "----------------"
    mkdir NFSTEST
    echo ""

    echo "mount -vv -t nfs -o resvport $TARGET:$MOUNTPOINT NFSTEST"
    echo "----------------"
    mount -vv -t nfs -o resvport $TARGET:$MOUNTPOINT NFSTEST
    MOUNT_OK="$?"
    echo ""

    if [[ "$MOUNT_OK" == 0 ]]
    then
        echo "ls -al NFSTEST"
        echo "----------------"
        ls -al NFSTEST
        echo ""
    
        echo "umount -v NFSTEST"
        echo "----------------"
        umount -v NFSTEST
        echo ""
    else
        echo "MOUNT failed"
        echo ""
    fi
    
    echo "rmdir"
    echo "----------------"
    rmdir NFSTEST
    echo ""
else
    echo "NFS Connection failed"
fi