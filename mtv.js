"use strict";

var PORT = 23456 ;

var PACKET_HEADER = new Buffer("55", 'hex');
var PACKET_FOOTER = new Buffer("AA", 'hex');
var PACKET_REPLY = new Buffer("000080", 'hex');
var PACKET_UNICAST = new Buffer("000040", 'hex');
var PACKET_BCAST = new Buffer("00FF3F", 'hex');

var Info_Model, Info_Name, Info_Version, Info_FWDate, Info_ID;
var MountPoint ;
var auto_start = false;
var debug_message = false;
var simulator_status = false;
var dgram = require('dgram');
var server = dgram.createSocket('udp4');

process.on( "SIGINT", function() {
  log('[SIGINT]');
  quit();
} );

server.on('listening', function () {
    server.setBroadcast(true);
    var address = server.address();
    log('Listening :' + address.port);
});

server.on('message', function (packet, remote) {
    // console.log(remote.address + ':' + remote.port +' - ' + packet.toString('hex'));

    var header, target, reply, len, payload, footer;
    var op_code, op_len;
    
    header = packet.slice(0,1);
    target = packet.slice(1,4);
    reply = packet.readInt8(4);
    len = packet.readInt16LE(5);
    payload = packet.slice(7, 7+len);
    footer = new Buffer([packet[7 + len]]);
    
    //if (header.equals(PACKET_HEADER) && footer.equals(PACKET_FOOTER)) {
    if (header.equals(PACKET_HEADER) && footer.equals(PACKET_FOOTER)) {
        // console.log( target.toString('hex') + ":" + reply + ":" + len + ":" + payload.toString('hex'));
        
        op_code = payload.readInt16LE(0);
        op_len = payload.readInt16LE(2);
        
        switch (op_code){
            case 1: // 0x0100
                log('R : PING' + ' from : ' + remote.address + ':' + remote.port + " target : " + target.toString('hex'));
                reply_Ping(remote);
                break;
            case 257: //0x0101
                log("R : NETWORK HDD Query" + ' from : ' + remote.address + ':' + remote.port + " target : " + target.toString('hex'));
                if (simulator_status) reply_HDD_Query(remote);
                break;
            case 33: //0x2100
                log('R : NET_HDD_READY - (0x2100 - 0x' + payload.slice(90,92).toString('hex') + ')' + ' from : ' + remote.address + ':' + remote.port + " target : " + target.toString('hex'));
                break;
            case 34: //0x2200
                log('R : SHUTDOWN - (0x2200 - 0x' + payload.slice(90,92).toString('hex') + ')' + ' from : ' + remote.address + ':' + remote.port + " target : " + target.toString('hex'));
                // Do thing
                break;
            case 37: //0x2500 
                log('R : STARTUP - (0x2500 - 0x' + payload.slice(90,92).toString('hex') + ')' + ' from : ' + remote.address + ':' + remote.port + " target : " + target.toString('hex'));
                break;
            default:
                log('op_code : ' + op_code + '(0x' + payload.slice(0,2).toString('hex') + ') , len : ' + op_len + ' from : ' + remote.address + ':' + remote.port + " target : " + target.toString('hex'));
        }
        
    } else {
        log("BAD Packet - " + packet.toString('hex'));
    }
});

process.stdin.on('readable',  function() {
  var chunk = process.stdin.read();
  if (chunk !== null) {
    // console.log(`command : ${chunk}`);
    switch (chunk){
        case 'm' :
            log('cmd : m');
            simulator_status = true;
            bcast_status('2100', '2d01');
            console.log('Server Started');
            break;
        case 's' :
            log('cmd : s');
            simulator_status = false;
            bcast_status('2200', '2d00');
            console.log('Server Stopped');
            break;
        case 'r' :
            if (simulator_status) bcast_status('2200', '2d00');
            load_config();
            if (simulator_status) bcast_status('2100', '2d01');
            console.log('Config Reloaded');
            break;
        case 'h' :
            print_help();
            break;
        case 'w' :
            console.log('Server Started ? - ' + simulator_status);
            console.log('Debug message ? - ' + debug_message);
            break;
        case 'd' :
            debug_message = !debug_message;
            console.log('Debug message : ' + debug_message);
            break;
        case 'q' :
        case '\x03' : // Ctrl+C
            console.log('QUIT');
            quit();
            break;
        case '\r': // ENTER
            console.log('');
            break;
        default :
            log(new Buffer(chunk).toString('hex'));
            if (!debug_message) print_help();
    }
  }
});


//function parsePacket(packet){
// 
//}
function reply_Ping(address){
    /*
    5500008040680004105a00
    4d545633323030530000000000000000
    4d5456333230305300d3a02e02000000187e5900c46d213008159f2e50d9802b
    392e303500000000000000000000000000000000
    b90b
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    2d01000026000600000bdf550a08aa
    */
    var header = new Buffer('5500008040680004105a00', 'hex');
    var opcode2 ;
    var footer = new Buffer('000026000600000bdf550a08aa', 'hex');
    
    if (simulator_status) {
        opcode2 = new Buffer('2d01', 'hex');
    } else {
        opcode2 = new Buffer('2d00', 'hex');
    }

    var packet = Buffer.concat([header, get_info_buffer(),opcode2, footer] );

    server.send(packet, 0, packet.length, address.port, address.address, function(err, bytes) {
    if (err) throw err;
    log('PING reply sent to ' + address.address +':'+ address.port + " ,opcode2 = " + opcode2.toString('hex'));
    //server.close();
    });
}

function reply_HDD_Query(address){
    var header = new Buffer('55000080400603011102030802', 'hex' );
    var payload1 = new Buffer(256).fill(0);
    var payload2 = new Buffer(256).fill(0);
    var payload3 = new Buffer(256).fill(0);
    var footer = new Buffer('AA', 'hex' );
    
    new Buffer(MountPoint).copy(payload1);
    new Buffer(MountPoint + "tv/").copy(payload2);
    new Buffer(MountPoint).copy(payload3);
    new Buffer('tv.dd').copy(payload3, MountPoint.length + 1);
    
    var packet = Buffer.concat([header, payload1, payload2,payload3, footer]);
    
    
    server.send(packet, 0, packet.length, address.port, address.address, function(err, bytes) {
    if (err) throw err;
    log('HDD Query reply sent to ' + address.address +':'+ address.port);
    //server.close();
    });
}

function bcast_status(op_code1, op_code2){
    // R : SHUTDOWN - (0x2200 - 0x2d08)
    var header = new Buffer('5500FF3F006800','hex');
    var opcode1_b = new Buffer(op_code1, 'hex');
    var size = new Buffer('5a00', 'hex');
    var opcode2_b = new Buffer(op_code2, 'hex');
    var footer = new Buffer('000026000600000bdf550a08aa', 'hex');
    
    var packet = Buffer.concat([header,opcode1_b,size, get_info_buffer(),opcode2_b , footer] );
    
    server.send(packet, 0, packet.length, PORT, '255.255.255.255', function(err, bytes) {
        if (err) throw err;
        log('BCAST system (' + op_code1 + ' , ' + op_code2 + ') to 255.255.255.255:'+ PORT);
    });
}

function print_help(){
    console.log('m : start server ; s : stop ; r : reload config');
    console.log('w : status ; d : toggle Debug ;q : quit ; h : help');
}

function log(message){
    if (debug_message) {
        
        var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
        var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);

        var now = localISOTime.replace('T', ' ').substr(0, 19);
        console.log(now + " - " + message);
    }
}



function quit(){
    
    if (simulator_status) {
        bcast_status("2200", "2d08");
    } else {
        bcast_status("2200", "2908");
    }
    
    setTimeout(function(){
        server.close();
        process.exit(0);
    }, 500);
}

function get_info_buffer(){
    var model = new Buffer(16).fill(0);
    new Buffer(Info_Model).copy(model);
    var name = new Buffer(32).fill(0);
    new Buffer(Info_Name).copy(name);
    var ver = new Buffer(20).fill(0);
    new Buffer(Info_Version).copy(ver);
    var date = new Buffer(Info_FWDate,'hex');
    var id = new Buffer(Info_ID, 'hex');
    
    return Buffer.concat([model, name, ver, date, id] );
    
}

function load_config(){
    log('Loading Config ...');

    var fs = require('fs');
    var ini = require('ini');
    var config = ini.parse(fs.readFileSync('./mtv.ini', 'utf8'));
    // console.dir(Object.keys(config));
    
    Info_Model = config.device.Model ;
    Info_Name = config.device.Name ;
    Info_Version = config.device.Version ;
    Info_FWDate = config.device.FWDate ;
    Info_ID = config.device.ID ;

    MountPoint = config.nfs.MountPoint ;
    
    auto_start = config.dev.AutoStart;
    debug_message = config.dev.Debug;
    
    log('   Info_Name : ' + Info_Name);
    log('   Info_ID : ' + Info_ID);
    log('   Info_Model : ' + Info_Model);
    log('   Info_Version : ' + Info_Version);
    log('   Info_FWDate : ' + Info_FWDate);
    log('   MountPoint : ' + MountPoint);
    log('   auto_start : ' + auto_start);
    log('   debug_message : ' + debug_message);
}

// Init
process.stdin.setEncoding('utf8'); 
process.stdin.setRawMode(true);
load_config();
server.bind(PORT, '0.0.0.0');
// AutoStart
if (auto_start) {
    simulator_status = true;
    bcast_status('2500', '2d01');
    bcast_status('2100', '2d01');
} else {
    bcast_status('2500', '2d00');
    bcast_status('2100', '2d00');
}
// Main
// Wait .5s for init
setTimeout(function(){
    console.log("");
    print_help();
}, 500);