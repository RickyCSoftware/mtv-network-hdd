# MTV Network HDD Server #

Simulate MagicTV Share server  
based on communication between MTV3200S(server) and MTV3000(client)  

## Version ##
1.0 - 2016-01-18 - init  
1.1 - 2016-01-23 - Add date to debug message ; add 'd' command ; add debug status in 'w'  
1.2 - 2016-01-27 - Proper Service Start & stop broadcast  
1.3 - 2016-02-01 - v0.12.9 compatible  

## Prerequisite ##

1. NFS Server  
2. Node ( Tested with v0.12.9, v4.2.4, v5.5.0 )  

## USAGE ##
1. git clone https://bitbucket.org/RickyCSoftware/mtv-network-hdd.git
2. cd mtv-network-hdd  
3. edit mtv.ini - Change "Name" and "MountPoint"  
4. node mtv.js  

## ISSUE / TODO ##
1. Need to restart client for any new record added to NAS by other recorder  
2. Find issue with Docker - see below

## ISSUE / TODO - DONE ##
1. Broadcast Start / Stop status without system shutdown  

## Docker ##
Find issue with Docker. Received packet is mangled.  
v1.9.1 - mangled broadcast message. MTV can still record to NAS  
v1.6.2 - mangled all received UDP message. Not working at all.  

## DONATE ##
You are granted a non-exclusive use of the Software for personal use only.  
Source code is provided for educational purposes and convenience.  
If you find it to be useful, please make a donation to support whale research -  
Ocean Alliance  - http://www.whale.org/  

## LICENSE ##

You are granted a non-exclusive use of the Software for personal use only.  
Source code is provided for educational purposes and convenience.  
Non-personal ( profit or not ) use is not allowed.  

All rights reserved

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN  
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR  
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.  

Copyright 2016 RickyC

All rights reserved